using DatesWebSite.Data;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using Xunit;

namespace XUnitTestProject
{
    public class UnitTest1
    {
        [Fact]
        public bool TestConnectionEF()
        {
            var options = new DbContextOptionsBuilder();
            options.UseNpgsql("server=manny.db.elephantsql.com;user=ycvbufhn;password=X5fS8iDPsJ-NHLidRk92MAx_vFY7wove;database=datesdb");
            using (var db = new ApplicationDbContext(options.Options, default(IOptions<OperationalStoreOptions>)))
            {
                try
                {
                    if (db.Database.CanConnect())
                    {
                        Console.WriteLine(@"INFO: ConnectionString: " + db.Database.ProviderName);
                            //+ "\n DataBase: " + db.Database.Connection.Database
                            //+ "\n DataSource: " + db.Database.Connection.DataSource
                            //+ "\n ServerVersion: " + db.Database.Connection.ServerVersion
                            //+ "\n TimeOut: " + db.Database.Connection.ConnectionTimeout);
                        return true;
                    }
                    return false;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
