import React, { Component } from 'react';
import "./Home.css"

export class Home extends Component {
    static displayName = Home.name;

    render() {
        return (
            <div>
                <div class="jumbotron jumbo-height">
                    <h1>Make dates with us</h1>
                </div>
                <div class="main-img d-flex flex-wrap align-items-end">
                    <img class="float-left" src="https://www.edarling.pl/wp-content/uploads/sites/26/2019/08/matching_2-1.jpg" />
                    <p>Meet your partner with us. We provide functionalites to meet your love as soon as possible.</p>
                </div>
            </div>
        );
    }
}
